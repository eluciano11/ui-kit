const path = require("path");

module.exports = {
  name: "UI-Kit Components",
  entry: "./lib/index.js",
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|\*\.stories\.js)/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  },
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist")
  }
};
