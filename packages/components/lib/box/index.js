import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from 'styled-components'

export const Box = styled.div`
  display: ${props => props.display};
  flex-direction: ${props => props.flexDirection};
  align-items: ${props => props.alignItems};
  justify-content: ${props => props.justifyContent};
  position: ${props => props.position};
  background-color: ${props => props.backgroundColor};
  height: ${props => props.height};
  width: ${props => props.width};
  padding: ${props => props.padding};
  border-radius: ${props => props.borderRadius};
  transition: ${props => props.transition}
`;

Box.propTypes = {
  display: PropTypes.oneOf(["block", "inline", "inline-block", "flex", "none"]).isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  flexDirection: PropTypes.oneOf(["row", "column", "row-reverse", "column-reverse"]),
  alignItems: PropTypes.oneOf(["flex-start", "flex-end", "center", "baseline", "stretch"]),
  justifyContent: PropTypes.oneOf(["flex-start", "flex-end", "center","space-between","space-around", "space-evenly"]),
  position: PropTypes.oneOf(["relative", "absolute", "float"]),
  backgroundColor: PropTypes.string,
  height: PropTypes.string,
  width: PropTypes.string,
  borderRadius: PropTypes.string,
  padding: PropTypes.string,
  transition: PropTypes.string
};

Box.defaultProps = {
  display: "block"
}
