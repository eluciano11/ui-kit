import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from 'styled-components';

import { colors } from "../styles-specs";

import { Box } from '../box';

const CIRCLE_SIZE = "34px";
const ANIMATION_TIME = "400ms";
const Circle = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  height: ${CIRCLE_SIZE};
  width: ${CIRCLE_SIZE};
  background-color: #FFFFFF;
  border-radius: 50%;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.2);
  left: ${props => props.isActive
    ? `calc(100% - (${CIRCLE_SIZE} + 3px))`
    : "3px"
  };
  transition: left ${ANIMATION_TIME} ease;
`;
const HiddenInput = styled.input`
  position: absolute;
  height: 0;
  width: 0;
  cursor: pointer;
  opacity: 0;
  margin: 0;
  left: 0;
  top: -10px;
`;
const Icons = styled.div`
  position: relative;
  height: 14px;
  width: 14px;

  ::before {
    content: "";
    display: block;
    position: absolute;
    top: 40%;
    width: 100%;
    height: 3px;
    border-radius: 3px;
    background-color: ${props => props.isChecked ? colors.success : colors.text.secondary};
    transform: ${props => props.isChecked ? "translateX(20%) translateY(0%) rotate(-50deg)" : "rotate(45deg)"};
    transition: transform ${ANIMATION_TIME} linear, background-color ${ANIMATION_TIME} linear;
  }

  ::after {
    content: "";
    display: block;
    position: absolute;
    top: 50%;
    width: ${props => props.isChecked ? "8px" : "100%"};
    height: 3px;
    border-radius: 3px;
    background-color: ${props => props.isChecked ? colors.success : colors.text.secondary};
    transform: ${props => props.isChecked ? "translateY(50%) rotate(42deg)" : "translateY(-50%) rotate(-45deg)"};
    transition: transform ${ANIMATION_TIME} linear, background-color ${ANIMATION_TIME} linear;
  }
`;


// Switch component
export class Switch extends PureComponent {
  static propTypes = {
    /** Callback to be called when the user clicks on the switch. */
    onClick: PropTypes.func.isRequired,
    /** State of the switch. */
    isChecked: PropTypes.bool,
    /** Flag to know if the switch is clickable or not. */
    isDisabled: PropTypes.bool
  };

  static defaultProps = {
    isChecked: false,
    isDisabled: false
  };

  state = {
    isChecked: this.props.isChecked
  };

  handleClick = () => {
    this.setState({ isChecked: !this.state.isChecked }, () => {
      this.props.onClick(this.state.isChecked);
    });
  };

  render() {
    const { isDisabled } = this.props;
    const { isChecked } = this.state;

    return (
      <label>
        <Box
          display="flex"
          alignItems="center"
          position="relative"
          height="40px"
          width="65px"
          borderRadius="100px"
          backgroundColor={isChecked ? colors.success : "#b6bfce"}
          transition={`background-color ${ANIMATION_TIME} ease`}
        >
          <Circle isActive={isChecked}>
            <Icons isChecked={isChecked} />
          </Circle>
          <HiddenInput
            type="checkbox"
            onChange={this.handleClick}
            disabled={isDisabled}
            checked={isChecked}
          />
        </Box>
      </label>
    );
  }
}
