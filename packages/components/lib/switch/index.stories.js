import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from "@storybook/addon-info";

import { Switch } from './index';

storiesOf('Switches', module)
  .add('Switch', withInfo()(() => {
    return (
      <Switch
        isChecked={false}
        onClick={action("clicked")}
        isDisabled={false}
      />
    )
  }));