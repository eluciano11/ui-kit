export const colors = {
  brand: "#f43151",
  primary: "#9c4fba",
  secondary: "#36038c",
  legacy: {
    primary: "#f73d56",
    secondary: "#787878"
  },
  text: {
    primary: "#2a3037",
    secondary: "#697280"
  },
  success: "#16d4af",
  placeholder: "#b6bfce"
};
