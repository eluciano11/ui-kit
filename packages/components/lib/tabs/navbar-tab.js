import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled, { ThemeProvider } from 'styled-components';

import { colors } from "../styles-specs";

const TabContainer = styled.div`
  margin: 0 20px;
`;

const Tab = styled.button`
  border: none;
  background-color: transparent;
  font-size: 16px;
  color: ${colors.text.primary};
  padding: 20px 0;
  text-align: center;
  border-bottom: ${props => props.isActive && `5px solid ${props.theme.primary}`};
  padding-bottom: ${props => props.isActive && "15px"};
`;

// Tab component used on the sections navbar.
export class NavbarTab extends PureComponent {
  static propTypes = {
    /** Title that will be displayed to the user. */
    title: PropTypes.string.isRequired,
    /** Flag to know if this tab is active or not. */
    isActive: PropTypes.bool.isRequired,
    /** Click callback */
    onClick: PropTypes.func.isRequired
  };

  static defaultProps = {
    theme: {
      primary: colors.primary,
      secondary: colors.secondary
    },
    title: "",
    isActive: false,
    onClick: () => {}
  };

  render() {
    return (
      <ThemeProvider theme={this.props.theme}>
        <TabContainer>
          <Tab isActive={this.props.isActive} onClick={this.props.onClick}>
            {this.props.title}
          </Tab>
        </TabContainer>
      </ThemeProvider>
    );
  }
}
