import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, object } from "@storybook/addon-knobs";
import { withInfo } from "@storybook/addon-info";

import { colors } from '../styles-specs'
import { NavbarTabs } from './index';

const items = [{
  label: 'Approved',
  id: 'approved'
}, {
  label: 'Pending',
  id: 'pending'
}, {
  label: 'Ignored',
  id: 'ignored'
}];

const stories = storiesOf('Tabs', module);

stories.addDecorator(withKnobs);
stories
  .add('Navbar', withInfo()(() => {
    return (
      <NavbarTabs
        theme={object("Theme", {
          primary: colors.primary,
          secondary: colors.secondary
        })}
        items={items}
        activeTab="approved"
        onClick={action("clicked")}
      />
    )
  }));