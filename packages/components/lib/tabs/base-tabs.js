import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import { Box } from "../box";

/**
 * Base component that encapsulates the tab functionality and delegates the
 * rendering UI components to the parent component.
 */
export class BaseTabs extends PureComponent {
  static propTypes = {
    /** Name or number to identify the tab that is active. */
    activeTab: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
      .isRequired,
    /** Render function that will receive props and render the UI. */
    children: PropTypes.func.isRequired,
    /** Callback function that has custom functionality for the parent component. */
    onClick: PropTypes.func
  };

  state = {
    activeTab: this.props.activeTab
  };

  /**
   * Update the state with the id of the tab selected and execute the onClick
   * callback if it's present.
   *
   * @param {string | number} tab - Selected tab.
   */
  onClick = tab => {
    this.setState({ activeTab: tab });

    if (this.props.onClick) {
      this.props.onClick(tab);
    }
  };

  render() {
    return (
      <Box
        display="flex"
        flexDirection="row"
        alignItems="center"
      >
        {this.props.children({
          activeTab: this.state.activeTab,
          onClick: this.onClick
        })}
      </Box>
    );
  }
}
