import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import { BaseTabs } from "./base-tabs";
import { NavbarTab } from "./navbar-tab";

export class NavbarTabs extends PureComponent {
  static propTypes = {
    /** Name or number to identify the tab that is active. */
    activeTab: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
      .isRequired,
    /** Tabs that will be rendered. */
    items: PropTypes.array.isRequired,
    /** Callback to execute custom functionality for each section */
    onClick: PropTypes.func
  };

  static defaultProps = {
    items: []
  };

  render() {
    return (
      <BaseTabs activeTab={this.props.activeTab} onClick={this.props.onClick}>
        {(props) => {
          return this.props.items.map((item) => (
            <NavbarTab
              key={item.id}
              title={item.label}
              isActive={props.activeTab === item.id}
              onClick={() => props.onClick(item.id)}
              theme={this.props.theme}
            />
          ))
        }}
      </BaseTabs>
    );
  }
}
