import babel from "rollup-plugin-babel";
import commonjs from "rollup-plugin-commonjs";
import resolve from "rollup-plugin-node-resolve";
import builtins from "rollup-plugin-node-builtins";
import { uglify } from "rollup-plugin-uglify";

export default {
  input: "lib/index.js",
  output: {
    name: "@ui-kit/components",
    file: "dist/bundle.js",
    exports: "named",
    format: "umd"
  },
  plugins: [
    babel({
      exclude: ["node_modules/**", "*.stories.js"]
    }),
    resolve({ preferBuiltins: true }),
    commonjs({
      include: 'node_modules/**',
      namedExports: {
        'node_modules/react/index.js': ['Component', 'PureComponent', 'Fragment', 'Children', 'createElement', 'cloneElement'],
        "node_modules/react-is/index.js": ["isValidElementType"]
      }
    }),
    builtins(),
    uglify({ sourcemap: true })
  ],
  treeshake: true
};
